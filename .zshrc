# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh
ZSH_CUSTOM=$HOME/.oh-my-zsh-custom

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="doshitan"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# FORTUNE_DISABLE="true"
COWSAY_FORTUNE="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(cabal encode64 fortune git rand-quote web-search yum z zsh-syntax-highlighting)

if [ -f ~/.zshrc.local.pre ]; then
  source ~/.zshrc.local.pre
fi

source $ZSH/oh-my-zsh.sh

# Customize to your needs...

# Local machine customizations/overrides
if [ -f ~/.zshrc.local.post ]; then
  source ~/.zshrc.local.post
fi
