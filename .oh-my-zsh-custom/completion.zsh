# Fuzzy matching for complete
zstyle ':completion:*' completer _complete _match _approximate
zstyle ':completion:*:match:*' original only
#zstyle ':completion:*:approximate:*' max-errors 1 numeric #Enable this OR one below
zstyle -e ':completion:*:approximate:*' \
        max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3))numeric)'
 
# Separate different types of possible completions and group them
zstyle ':completion:*:descriptions' format "- %d -"
zstyle ':completion:*:corrections' format "- %d - (errors %e})"
zstyle ':completion:*:default' list-prompt '%S%M matches%s'
zstyle ':completion:*' group-name ''
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*' verbose yes

# Other completion config
zstyle ':completion:*:functions' ignored-patterns '_*' # Ignore completion functions for commands you don't have
