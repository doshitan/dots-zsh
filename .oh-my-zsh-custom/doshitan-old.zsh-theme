PROMPT="%(#.%{${fg[red]}%}.%{${fg[green]}%})%n%{${fg[default]}%}@%{${fg[green]}%}%M%{${fg[default]}%} [%35<...<%~] %(#.%{${fg[red]}%}.%{${fg[green]}%})>%{${fg[default]}%} "

RPROMPT="%(?.%{${fg[green]}%}O.%{${fg[red]}%}X) %{${fg[default]}%}%*"

PS4='+%N:%i:%_>'
