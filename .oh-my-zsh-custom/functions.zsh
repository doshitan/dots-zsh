# Calculator 1
# calc () { echo $* |bc -l }

# Calculator 2
# Here are some quick calculators that output in integer
# hexadecimal, decimal, and binary. See http://www.zsh.org/mla/users/2003/msg00163.html for more info and usage examples
# Enter 2#1010 for 1010 to be taken as a binary number, similar syntax for other bases besides 10, i.e, 8#, 16#
zcalc ()  { print $(( ans = ${@:-ans} )) } # Outputs an integer number
zcalch () { print $(( [#16] ans = ${@:-ans} )) } # Outputs a hexadecimal number
zcalcd () { print $(( [#10] ans = ${@:-ans} )) } # Outputs a decimal number
zcalco () { print $(( [#8] ans = ${@:-ans} )) } # Outputs an octanary number
zcalcb () { print $(( [#2] ans = ${@:-ans} )) } # Outputs a binary number
zcalcasc () { print $(( [#16] ans = ##${@:-ans} )) } # Outputs the hex value of an ASCII character

# Calculator 3
# calc () { awk "BEGIN{ print $*}" ;}


# mkdir, cd into it
mkcd () {
  mkdir -p "$*"
  cd "$*"
}

# Display some machine details
system_info () {
  echo -e "\nMachine information:"; uname -a
  echo -e "\nUsers logged on:"; w -h
  echo -e "\nCurrent date:"; date
  echo -e "\nMachine status:"; uptime
  echo -e "\nMemory status:"; free -h
  echo -e "\nFilesystem status:"; df -h
}

shell_info () {
  echo -e "whoami: $(whoami)"
  echo -e "SHLVL: $SHLVL"
  echo -e "SHELL: $SHELL"
  echo -e "pwd: $(pwd)"
}

# Display spacetime information
spacetime () {
  echo
  cal -3
  echo "Local: $(date) ($(date -Iseconds))"
  echo "UTC: $(date --utc) ($(date --utc -Iseconds))"
  command -v ddate &> /dev/null && echo "Discordian: $(ddate)"

  local default_route_info=$(ip route get 1)
  local default_route_src_ip=$(echo $default_route_info | sed -n 's/^.*src \([0-9.]*\) .*$/\1/p')
  local default_route_device=$(echo $default_route_info | sed -n 's/^.*dev \(.*\) src .*$/\1/p')

  echo
  echo -e "Internal IP: $default_route_src_ip ($default_route_device)"
  echo -en "External IP: "

  if command -v dig &> /dev/null; then
    # generally faster, so prefer this approach if possible
    dig +short myip.opendsn.com @resolver1.opendns.com
  else
    local external_ip=$(curl -s ifconfig.me)
    # IP options:
    # ifconfig.me
    # www.icanhazip.com
    # ipecho.net/plain
    # ident.me
    # bot.whatismyipaddress.com
    # diagnostic.opends.com/myip
    # checkip.amazonaws.com
    echo $external_ip
  fi

  echo

  echo "Geocoded IP info:"

  geo_ip_info=$(curl -s http://ip-api.com/json/$external_ip)

  if command -v jq &> /dev/null; then
    echo $geo_ip_info | jq
  else
    echo $geo_ip_info | sed 's/,"/,\n"/g'
    echo
  fi
}

timestamp_to_date () {
  for d in $*; do
    local iso=$(date -d "@$d" -Iseconds)
    local default=$(date -d "@$d")
    echo "$iso | $default"
  done
}

# Make a backup copy
bak () {
  for f in $*; do
    cp "$f"{,.bak}
  done
}

# Replace changed file with backup copy
ubak () {
  for f in $*; do
    cp "$f"{.bak,}
  done
}

# Replace changed file with backup copy, remove backup copy
urbak () {
  for f in $*; do
    cp "$f"{.bak,}
    rm "$f.bak"
  done
}

# diff with backup copy
diffbak () {
  for f in $*; do
    diff "$f"{,.bak}
  done
}

# Reminder
reminder () {
  utimer -c $1
  if [[ $? = 0 ]]; then
    espeak "$argv[2,$#]"
  else
    echo "Timer exited with an error"
  fi
}

# Word Count TeX
wctex () {
  detex "$*" | wc
}

latestNixpkgs () {
  local channel=${1:-nixos-unstable-small}
  local latest_sha=$(git ls-remote https://github.com/NixOS/nixpkgs-channels.git "$channel" | cut -f1)
  local latest_sha_hash=$(nix-prefetch-url --unpack "https://github.com/NixOS/nixpkgs/archive/$latest_sha.tar.gz" | tail -n 1)
  echo "For $channel:"
  echo ", rev ? \"$latest_sha\""
  echo ", sha256 ? \"$latest_sha_hash\""
}

weather () {
  curl wttr.in/$1
}
