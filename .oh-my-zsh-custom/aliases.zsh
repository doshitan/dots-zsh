# Listing, use -r to reverse sort order
alias l='ls -CF'
alias la='ls -AF'
alias lss='ls -S' # Sort by size
alias ll='ls -lh' # Long list format, human readable sizes
alias lld='ls -lh --group-directories-first'
alias lls='ll -S'
alias llt='ll -t' # Sort by modification time, newest first
alias llu='llt -u' # Sort by access time, newest first
alias lla='ll -A'
alias ls_perms='stat -c "%A (%a) %n"' # List permissions in human-readable and octal format

# Easy Searching
alias gh='fc -l 0 | grep' # grep history
alias pse='ps -ef | grep' # grep current processes
alias ga='alias | grep -i' # grep aliases

# Aliases for terminal calculator
## Use this if you are using Calculator 1
## alias c='calc'

## Use the following if you are using Calculator 2 (default); See http://www.zsh.org/mla/users/2003/msg00163.html for more info
alias c='noglob zcalcd' # Calculates decimal numbers, noglob so * works for multiplication
alias ch='noglob zcalch' # Calculates hexadecimal numbers
alias ci='noglob zcalc' # Calculates integer numbers
alias co='noglob zcalco' # Calculates octanary numbers
alias cb='noglob zcalcb' # Calculates binary numbers
alias casc='noglob zcalcasc' # Calculates ASCII value of a character, returns hex value

# Easy timers
alias stopwatch='utimer -s'
alias countdown='utimer -c'
alias timer='utimer -t'

# Misc
alias duc='du -sh `ls -d *`' # Show directory usage for all directories in the current dir
alias sulast='sudo !!' # Execute previous command as root
alias sush='sudo -s' # Switch to a root shell
alias mostusedcmds='history | awk '"'"'{print $2}'"'"' | sort | uniq -c | sort -rn | head -10'
alias readhistory='fc -R'   # Read the history file (share history from other shells)
alias removebrokensymlinks='rm *(-@)'

# Easy /proc stuff
alias cpuinfo='cat /proc/cpuinfo'
alias meminfo='cat /proc/meminfo'
alias zoneinfo='cat /proc/zoneinfo'
alias mounts='cat /proc/mounts'
alias swappiness='cat /proc/sys/vm/swappiness'
alias scheduler='cat /sys/block/sda/queue/scheduler'

# Spelling
alias spell='hunspell'
alias spelltex='hunspell -l -t -i utf-8'

# Globals
alias -g G='grep'
alias -g :g='| grep' # Easy grepping
alias -g :gv='| grep -v'
alias -g :p='| $PAGER' # Easy piping one
alias -g :sp='| sort | $PAGER' # Easy piping two
alias -g :n='&> /dev/null' # Easy piping to /dev/null
alias -g :sn='1> /dev/null' # See above
alias -g :en='2> /dev/null' # Ditto
alias -g :nl='| nl' # Number output lines
alias -g :xc='| xclip'
alias -g :c='| wl-copy'
alias -g :cf='| wl-copy -t text/html'


# http://www.commandlinefu.com/commands/view/2531/matrix-style
alias thematrix='echo -ne "\e[32m" ; while true ; do echo -ne "\e[$(($RANDOM % 2 + 1))m" ; tr -c "[:print:]" " " < /dev/urandom | dd count=1 bs=50 2> /dev/null ; done'

# http://www.linuxnix.com/2013/02/find-determine-if-a-machine-runs-64-bit-linux-os-or-not.html
# http://www.cyberciti.biz/faq/linux-how-to-find-if-processor-is-64-bit-or-not/
alias cpuBits='cat /proc/cpuinfo | grep -q "\blm\b" && echo "64-bit" || echo "32-bit"'
alias osBits='uname -a | grep -q "\bx86_64\b" && echo "64-bit" || echo "32-bit"'

alias e='emacsclient -t'
alias en='emacs -nw'

alias o='xdg-open'

alias strip_photo='exiftool -geotag= -EXIF= -tagsFromFile @ -exif:Orientation'
alias strip_photo_inplace='exiftool -overwrite_original -geotag= -EXIF= -tagsFromFile @ -exif:Orientation'
# TODO maybe one that renames the file as well?
