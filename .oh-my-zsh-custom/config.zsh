export ALTERNATE_EDITOR=""
export EDITOR="emacsclient -t"
export VISUAL="emacsclient -c -a emacs"

# Extended Glob
setopt extended_glob
# Only return error if none of the patterns match
setopt csh_null_glob
# Treat digits as numbers, not strings, so consecutively numbered files are
# listed in number order (can be specified for one pattern only with the
# qualifier n, e.g., echo *(n))
setopt numeric_glob_sort

# Make shell display actual directory when following a symbolic link
setopt chase_links

# Don't overwrite existing files (like when using >)
setopt no_clobber
# Just press Up Arrow if we really want to clobber file
setopt hist_allow_clobber

# Do not notify when background processes complete, could be interrupting/annoying
setopt no_notify

# Command run time
REPORTTIME=5
TIMEFMT="%U user %S system %P cpu %*Es total"

# Turn off TTY 'start' and 'stop' commands in interactive shells. They default
# to C-q and C-s, but Bash (and ZSH) use C-s to do forward history search. The
# start/stop behavior has never been useful to me, if it ever becomes useful,
# then find different keys to bind the behavior to.
# See: http://stackoverflow.com/q/14348995
if [ ! -z "$PS1" ]; then # is interactive
  stty start ''
  stty stop ''
  stty -ixon
  stty ixoff
  stty ixany # let any character restart output, not only start character
fi

# -----------------------------------------------------
# Setup PATH
# -----------------------------------------------------
INCLUDE_SUPERUSER_FUNCTIONS="true"
if [ "$INCLUDE_SUPERUSER_FUNCTIONS" = "true" ]; then
  PATH="$PATH:/usr/local/sbin:/usr/sbin:/sbin"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ]; then
  PATH="$HOME/.local/bin:$PATH"
fi

# pickup Doom Emacs tools
if [ -d "$HOME/.emacs.d/bin" ]; then
  PATH="$HOME/.emacs.d/bin:$PATH"
fi

# remove duplicate entries
typeset -U PATH

# -----------------------------------------------------
# Setup highlighter
# -----------------------------------------------------
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)

# -----------------------------------------------------
# Setup ccache
# -----------------------------------------------------
CCACHE_DIR=/tmp/ccache
CCACHE_COMPRESS=true

# -----------------------------------------------------
# Setup better color support for ls
# -----------------------------------------------------
if [ -f "$HOME/.dir_colors" ] ; then
  eval $(dircolors -b "$HOME/.dir_colors")
fi

# -----------------------------------------------------
# Setup nix
# -----------------------------------------------------
if [ -e "$HOME/.nix-profile/etc/profile.d/nix.sh" ]; then
  source "$HOME/.nix-profile/etc/profile.d/nix.sh"
fi

if [ -e "$HOME/.nix-profile/lib/aspell" ]; then
  export ASPELL_CONF="dict-dir $HOME/.nix-profile/lib/aspell"
fi

# -----------------------------------------------------
# Setup arcanist completion
# -----------------------------------------------------
# if command -v arc > /dev/null 2>&1; then
#   source "$(command -v arc | xargs readlink -f | xargs dirname | xargs dirname)/libexec/arcanist/resources/shell/bash-completion"
# fi

# -----------------------------------------------------
# Setup direnv if present
# -----------------------------------------------------
if command -v direnv > /dev/null 2>&1; then
  eval "$(direnv hook zsh)"
fi
