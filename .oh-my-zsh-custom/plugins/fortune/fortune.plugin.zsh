# Display a fortune message when launching a new shell, optionally passing it
# through cowsay/cowthink
#
# Dependencies:
# - fortune, the package nowdays is usually called fortune-mod
# - cowsay, if you want fortunes in speach/thought bubbles from strange creatures
# - lolcat, if you want color
#
# FORTUNE_DISABLE when set will disable printing a fortune automatically
#
# FORTUNE_ARGS specifies arguments to pass to fortune
#
# COWSAY_FORTUNE determines if we use cowsay
# COWTHINK_FORTUNE determines if we use cowthink
# COWSAY_FORTUNE_COWFILE determines what cowfile is used
# COWSAY_ARGS specifies arguments to pass to cowsay/cowthink
#
# FORTUNE_COLOR determines if output is piped through lolcat
# FORTUNE_COLOR_ARGS specifies arguments to pass to lolcat
#
# For example:
#
# COWSAY_FORTUNE="true"
# COWSAY_FORTUNE_COWFILE="tux"
#
# Would draw the fortune inside a Tux's talking bubble
#
# TODO:
#   * Check that the cowfile name provided is available before using it
#   * Implement option to only show one fortune a day instead of on every new
#     shell
#   * Option to pull from rand-quote plugin's "quote" command in addition (or
#     instead of?) fortune?

random_cowfile() {
  cowsay -l | grep -v "Cow files in" | sed "s/ /\n/g" | shuf -n1
}

function fortune_plugin() {
  local cmd

  : "${FORTUNE_ARGS:='-a'}"
  local frtnCmd="fortune ${FORTUNE_ARGS}"

  cmd+=$frtnCmd

  local cowCmd
  if [ "$COWSAY_FORTUNE" = "true" ]; then
    cowCmd="cowsay"
  elif [ "$COWTHINK_FORTUNE" = "true" ]; then
    cowCmd="cowthink"
  fi

  # Forcing -n as it disables cowsay's word-wrapping because fortune's are
  # generally short, already come wrapped at a reasonable column width, and
  # often have important whitespace formatting
  cowCmd+=" -n $COWSAY_ARGS"

  if [ -n "$cowCmd" ]; then

    if [ ! -n "$COWSAY_FORTUNE_COWFILE" ] || [ "$COWSAY_FORTUNE_COWFILE" = "random" ]; then
      cowCmd+=" -f $(random_cowfile)"
    else
      cowCmd+=" -f $COWSAY_FORTUNE_COWFILE"
    fi

    cmd+=" | $cowCmd"
  fi

  if [ "$FORTUNE_COLOR" = "true" ]; then
    : "${FORTUNE_COLOR_ARGS:='--spread=1'}"
    cmd+=" | lolcat $FORTUNE_COLOR_ARGS"
  fi

  eval $cmd
}

if [ -z "$FORTUNE_DISABLE" ]; then
  fortune_plugin
fi
